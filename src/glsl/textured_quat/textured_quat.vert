#version 400 core

vec2 positions[] = vec2[](
    vec2(1.,  1.),
    vec2(1., -1.),
    vec2(-1., -1.),
    vec2(-1.,  1.)
);

vec2 uvs[] = vec2[](
    vec2(1., 1.),
    vec2(1., 0.),
    vec2(0., 0.),
    vec2(0., 1.)
);

uint indices[] = uint[](
    3, 1, 0,
    3, 2, 1
);

out vec2 tex_coords;

void main()
{
    tex_coords = uvs[indices[gl_VertexID]];
    gl_Position = vec4(positions[indices[gl_VertexID]], 0., 1.);
}