#version 400 core

layout (location = 0) out vec4 out_color;

in vec2 tex_coords;
    
uniform sampler2D quad_texture;
    
void main()
{               
    out_color = texture(quad_texture, tex_coords);
}

