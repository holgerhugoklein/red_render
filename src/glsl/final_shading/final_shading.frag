#version 450 core
#define TILE_DIM 16
#define MAX_LIGHTS_TILE 1024

layout (location = 0) out vec4 out_color;

uniform vec2 screen_dims;

layout(std430, binding = 0) readonly buffer point_lights_buffer {
	vec4 point_lights_pos_and_rad[];
};

layout(std430, binding = 1) readonly buffer light_indices_per_tile_buffer {
	int in_light_indices[];
};

in V_Data
{
    vec3 pos_in_world;
    vec3 normal_in_world;
} v_in;

float rand(vec2 co) {
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}


vec3 addPointLight(vec4 pos_and_rad, int idx)
{
    vec3 dir_to_light = pos_and_rad.xyz - v_in.pos_in_world;

    float dist_to_light = length(dir_to_light);    
    if (dist_to_light > pos_and_rad.w) return vec3(0.);

    dir_to_light = dir_to_light / dist_to_light;
    float angle = dot(v_in.normal_in_world, dir_to_light);

    angle = max(angle, 0.);
        
    float r1 = rand(vec2(idx, rand(vec2(idx, idx))));
    float r2 = rand(vec2(r1, rand(vec2(r1, r1))));
    float r3 = rand(vec2(r2, rand(vec2(r2, r2))));

    float light_bulb_radius = .1;
    float light_bulb_radius_sq = light_bulb_radius * light_bulb_radius;

    vec3 c = vec3(r1, r2, r3);
    c = vec3(.5, .3, .2);

    return 2 / (light_bulb_radius_sq) * (1 - (dist_to_light / length(vec2(dist_to_light, light_bulb_radius)))) * angle * c;
}

void main()
{
    ivec2 pixel_location = ivec2(gl_FragCoord.xy);
    ivec2 tile_id = pixel_location / ivec2(TILE_DIM);
    ivec2 amt_tiles = ivec2(ceil(screen_dims / float(TILE_DIM)));
    int base_index = (tile_id.y * amt_tiles.x + tile_id.x) * MAX_LIGHTS_TILE;

    vec3 total_light = vec3(0);

    for(uint i = 0; i < MAX_LIGHTS_TILE; ++i)
    {
        int light_index = in_light_indices[i + base_index];

        if(light_index == -1) break;
        
        vec4 pos_and_rad = point_lights_pos_and_rad[light_index];
        total_light += addPointLight(pos_and_rad, light_index);
    }

    vec3 color = vec3(.3, .2, .4);

    vec3 ambient = vec3(.3);

    out_color.rgb = color * (total_light + ambient);

    float exposure = .3;
    out_color.rgb = vec3(1.0) - exp(-out_color.rgb * exposure);
    out_color.a = 1.;   
}