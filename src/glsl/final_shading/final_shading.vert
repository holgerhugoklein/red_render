#version 400 core

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normal;
layout (location = 2) in mat4 a_instance_data;

uniform mat4 view;
uniform mat4 projection;


out V_Data
{
    vec3 pos_in_world;
    vec3 normal_in_world;
} v_out;

void main()
{
    mat3 rot = mat3(a_instance_data);
    v_out.normal_in_world = rot * a_normal;
    v_out.pos_in_world = rot * a_position + vec3(a_instance_data[3]);
    gl_Position = projection * view * vec4(v_out.pos_in_world, 1.);
}