#version 400 core

in vec3 a_position;
in mat4 a_per_instance_mat4;

uniform mat4 view;
uniform mat4 projection;

out vec4 pos_in_view;

void main()
{
    mat3 rot = mat3(a_per_instance_mat4);
    vec3 pos_in_world = rot * a_position + vec3(a_per_instance_mat4[3]);
    pos_in_view = view * vec4(pos_in_world, 1.);
    gl_Position = projection * pos_in_view;
}