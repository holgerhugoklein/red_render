#version 400 core
layout (location = 0) out vec4 out_color;

in vec4 pos_in_view;

void main()
{
    out_color = vec4(pos_in_view.z, 0, 0, 1.);
}