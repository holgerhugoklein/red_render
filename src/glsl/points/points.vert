#version 330 core
layout (location = 0) in vec4 a_pos_and_rad;

uniform mat4 projection;
uniform mat4 view;

void main() {
    gl_Position = projection * view *  vec4(a_pos_and_rad.xyz, 1.0);
}
   