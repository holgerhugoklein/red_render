#include <stdio.h>
#include <assert.h>

#include <red_filesystem/win32_file_system.h>

#include "sdl_mainloop.h"

#include "render/opengl/win32_opengl.h"
#include "assets/assets.h"
#include "assets/assimp_load_model.h"
#include "assets/model_paths.h"

int main(int argc, char **argv)
{
    f32 w = 1920;
    f32 h = 1080;
    SDL_Window *window = RED::render::initAndCreateSDLWindow("red_render_opengl", 1920, 1080);
    assert(window);
    RED::assets::setup();
    
    RED::sdl_mainloop(window, w, h);
    return 1;
}
