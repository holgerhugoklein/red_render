#pragma once

#include <glm/glm.hpp>

#include <sdl2/SDL.h>

#include <typedefs.h>

#include "input.h"

namespace RED
{
    namespace input
    {
        struct SDL_Input
        {
            glm::vec2 mouseLastFrame;  
        };

        SDL_Input setup()
        {
            i32 x;
            i32 y;
            u32 mouse_state = SDL_GetMouseState(&x, &y);
            return {{x, y}};
        }

        Input generateFromSDL(SDL_Input *sdl_input)
        {
            Input input = {};

            const u8* key_state = SDL_GetKeyboardState(0);

            if(key_state[SDL_SCANCODE_W])
            {
                input.moveCmds[(u32) MOVE_DIR::FORWARD] = {1.};   
            }
            if(key_state[SDL_SCANCODE_S])
            {
                input.moveCmds[(u32) MOVE_DIR::BACKWARD] = {1.};   
            }
            if(key_state[SDL_SCANCODE_A])
            {
                input.moveCmds[(u32) MOVE_DIR::LEFT] = {1.};   
            }
            if(key_state[SDL_SCANCODE_D])
            {
                input.moveCmds[(u32) MOVE_DIR::RIGHT] = {1.};   
            }
            if(key_state[SDL_SCANCODE_ESCAPE])
            {
                input.stop_running = true;   
            }

            i32 x;
            i32 y;
            u32 mouse_state = SDL_GetMouseState(&x, &y);
            glm::vec2 mouse_this_frame{x, y};
            glm::vec2 dmouse = mouse_this_frame - sdl_input->mouseLastFrame;
            sdl_input->mouseLastFrame = mouse_this_frame;

            if(mouse_state & SDL_BUTTON_LMASK)
            {               
                input.rotCameraCmds = { dmouse };
            }

            SDL_Event event = {};
            while (SDL_PollEvent(&event))
            {                
                if (event.type == SDL_QUIT)
                {
                    input.stop_running = true;
                }
            }

            return input;
        }
    }
}