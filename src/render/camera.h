#pragma once

#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../assets/assets.h"

namespace RED
{
    namespace render
    {
        namespace cam 
        {
            const glm::vec3 WORLD_UP{0., 1., 0.};

            struct Camera
            {
                glm::vec3 pos;
                glm::vec3 view_dir;
                glm::vec3 right;
                glm::vec3 up;
                f32 fovy;
                f32 w;
                f32 h;
            };

            Camera setupCamera(glm::vec3 pos, glm::vec3 view_dir, f32 fovy, f32 w, f32 h)
            {
                Camera camera = {};
                camera.pos = pos;
                camera.view_dir = glm::normalize(view_dir);
                camera.right = glm::normalize(glm::cross(view_dir, WORLD_UP));
                camera.up = glm::normalize(glm::cross(camera.right, camera.view_dir));
                camera.fovy = fovy;
                camera.w = w;
                camera.h = h;
                return camera;
            }

            glm::mat4 genLookAt(Camera *camera)
            {
                return glm::lookAt(camera->pos, camera->pos + camera->view_dir, camera->up);
            }

            glm::mat4 genProj(Camera *camera)
            {
                return glm::perspective(camera->fovy, camera->w / camera->h, .1f, 100.f);
            }

            void move(Camera *camera, glm::vec4 dirs, f32 dt)
            {
                if(glm::length(dirs) == 0) return;
                
                glm::vec4 dirsNorm = glm::normalize(dirs);

                dirsNorm *= dt;

                camera->pos += camera->view_dir * dirsNorm[0];
                camera->pos -= camera->view_dir * dirsNorm[1];
                
                camera->pos -= camera->right * dirsNorm[2];
                camera->pos += camera->right * dirsNorm[3];
            }

            void rotate(Camera *camera, glm::vec2 xy, f32 dt)
            {
                if(glm::length(xy) == 0) return;

                glm::vec2 xyNorm = glm::normalize(xy);
                xyNorm *= dt;

                glm::mat4 rot = glm::rotate(glm::mat4{1.}, -xyNorm.x, camera->up);
                camera->view_dir = glm::normalize(glm::mat3(rot) * camera->view_dir);
                camera->right = glm::normalize(glm::cross(camera->view_dir, WORLD_UP));

                rot = glm::rotate(glm::mat4{1.}, -xyNorm.y, camera->right);
                camera->view_dir = glm::normalize(glm::mat3(rot) * camera->view_dir);
                camera->up = glm::normalize(glm::cross(camera->right, camera->view_dir));
            }
        }        
    }
}