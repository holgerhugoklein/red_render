#pragma once

#include <sdl2/SDL.h>

#include "scene.h"
#include "../assets/model_3d.h"

namespace RED
{
    namespace render
    {
        void render_drawSceneToBackbufferAndPresent(scene::Scene *scene, SDL_Window *window);
        void render_pushMeshOntoGPU(assets::MODELS model, assets::Mesh *mesh);
    }
}