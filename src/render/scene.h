#pragma once

#include <vector>
#include <unordered_map>

#include "../assets/assets.h"

#include "camera.h"

namespace RED
{
    namespace render
    {
        namespace scene 
        {
            struct Scene
            {
                std::vector<assets::MODELS> models_in_scene;
                std::unordered_map<assets::MODELS, std::vector<glm::mat4>> model_instances;           
                std::vector<glm::vec4> pointlight_pos_and_radius; 
                std::vector<glm::vec4> pointlight_colors;
                cam::Camera camera;
            };

            void addModel(Scene *scene, assets::MODELS model)
            {
                std::vector<glm::mat4> mats;
                mats.reserve(512);
                scene->model_instances.insert({model, mats});
                scene->models_in_scene.push_back(model);
            }

            void pushModelInstance(Scene *scene, assets::MODELS model, glm::mat4 transformation)
            {
                scene->model_instances[model].push_back(transformation);
            }
        }        
    }
}