#pragma once

#include <typedefs.h>

#include "./program.h"

namespace RED
{
    namespace render 
    {
        namespace glsl_paths
        {
            namespace depth_prepass {
                const char *vert = "./src/glsl/depth_prepass/depth_prepass.vert";
                const char *frag = "./src/glsl/depth_prepass/depth_prepass.frag";
            }

            namespace final_shading {
                const char *vert = "./src/glsl/final_shading/final_shading.vert";
                const char *frag = "./src/glsl/final_shading/final_shading.frag";
            }

            namespace textured_quat {
                const char *vert = "./src/glsl/textured_quat/textured_quat.vert";
                const char *frag = "./src/glsl/textured_quat/textured_quat.frag";
            }

            namespace points {
                const char *vert = "./src/glsl/points/points.vert";
                const char *frag = "./src/glsl/points/points.frag";
            }


            namespace forward_plus {
                const char *light_culling = "./src/glsl/forward_plus/light_culling.comp";
            }
        }
    }
}