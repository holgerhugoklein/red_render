#pragma once

#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

#include <typedefs.h>

#include "./program.h"
#include "./util.h"
#include "./glsl_paths.h"

#include "../../assets/model_3d.h"

namespace RED
{
    namespace render 
    {
        namespace ogl
        {
            namespace mem
            {
                namespace buf
                {
                    template <typename T>
                    struct Non_Dynamic_Buffer
                    {
                        u32 id;
                        u32 cap_in_T;
                        u32 size_in_T;
                    };

                    struct Per_Instance_Buffer
                    {
                        u32 id;
                        u32 cap_in_mat4;
                        u32 size_in_mat4;
                        glm::mat4 *ptr;
                    };         

                    template<typename T>
                    Non_Dynamic_Buffer<T> generateNonDynamicBuffer(u32 cap_in_T, u32 buffer_type)
                    {
                        u32 id;
                        glGenBuffers(1, &id);
                        glBindBuffer(buffer_type, id);
                        u32 size_in_bytes = cap_in_T * sizeof(T);
                        glBufferData(buffer_type, size_in_bytes, 0, GL_STATIC_DRAW);
                        return { id, cap_in_T, 0};
                    }               

                    Per_Instance_Buffer generatePerInstanceBuffer(u32 cap_in_mat4)
                    {
                        u32 id;
                        glGenBuffers(1, &id);
                        glBindBuffer(GL_ARRAY_BUFFER, id);
                        u32 flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
                        u32 size_in_bytes = cap_in_mat4 * sizeof(glm::mat4);
                      
                        glBufferStorage(GL_ARRAY_BUFFER, size_in_bytes, 0, flags);
                        glm::mat4 *ptr = (glm::mat4*) glMapBufferRange(GL_ARRAY_BUFFER, 0, size_in_bytes, flags);                        

                        return {id, cap_in_mat4, 0, ptr};                       
                    }

                    template <typename T>
                    u32 pushDataOntoNonDynamicBufferReturnOffset( Non_Dynamic_Buffer<T> *buffer, T* data, u32 amt, u32 buffer_type)
                    {
                        assert(buffer->size_in_T + amt < buffer->cap_in_T);
                        u32 size_in_bytes = amt * sizeof(T);
                        glBindBuffer(buffer_type, buffer->id);
                        glBufferSubData(buffer_type, buffer->size_in_T * sizeof(T), size_in_bytes, data);
                        buffer->size_in_T += amt;
                        return buffer->size_in_T - amt;
                    }

                    u32 pushPerInstanceDataReturnOffset(Per_Instance_Buffer* buffer, glm::mat4* data, u32 amt)
                    {
                        assert(buffer->size_in_mat4 + amt < buffer->cap_in_mat4);
                        u32 size_in_bytes = amt * sizeof(glm::mat4);
                        memcpy(buffer->ptr + buffer->size_in_mat4, data, size_in_bytes);
                        buffer->size_in_mat4 += amt;
                        return buffer->size_in_mat4 - amt;
                    }
                }
            }
        }
    }
}