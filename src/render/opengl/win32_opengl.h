#pragma once

#define GLEW_NO_GLU
#include <GL/glew.h>
#include <sdl2/SDL.h>
#include <sdl2/SDL_opengl.h>

#include <typedefs.h>

#include "../scene.h"

#include "opengl.h"

namespace RED
{
    namespace render
    {
        SDL_Window * initAndCreateSDLWindow(const char *title, f32 w, f32 h)
        {
            SDL_Init(SDL_INIT_EVERYTHING);
            SDL_Window *window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_OPENGL);
            SDL_GLContext gl_context = SDL_GL_CreateContext(window);
            ogl::setupAfterContextCreation(w, h);
            return window;
        }

        void render_drawSceneToBackbufferAndPresent(scene::Scene *scene, SDL_Window *window)
        {
            ogl::drawSceneToBackbuffer(scene);
            SDL_GL_SwapWindow(window);
        }        

        void render_pushMeshOntoGPU(assets::MODELS model, assets::Mesh *mesh)
        {   
            ogl::mem::pushMeshOntoGPUMemory(model, mesh);
        }
    }    
}
