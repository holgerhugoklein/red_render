#pragma once

#include <assert.h>
#include <vector>

#include <GL/glew.h>

#include <red_filesystem/file_system.h>

#include "./program.h"

namespace RED
{
    namespace render 
    {
        namespace ogl
        {
            namespace util
            {
                i32 compileShader(const char* code, u32 type)
                {
                    u32 shader = glCreateShader(type);
                    glShaderSource(shader, 1, &code, 0);
                    glCompileShader(shader);
                    i32 isCompiled;
                    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
                    if (isCompiled == GL_FALSE)
                    {
                        const i32 logLen = 512;
                        char log[logLen];
                        glGetShaderInfoLog(shader, logLen, (GLsizei*)&logLen, log);
                        printf("Error creating shader %s %s\n", code, log);
                        return -1;
                    }

                    return shader;
                }

                i32 linkProgram(std::vector<u32> shader_ids)
                {
                    u32 program = glCreateProgram();
                    for (u32 shader_id : shader_ids)
                    {
                        glAttachShader(program, shader_id);
                    }
                    
                    for(u32 i = 0; i < (u32) SHADER_ATTR::AMT; ++i){
                        glBindAttribLocation(program, i, attr_names[i]);
                    }

                    glLinkProgram(program);

                    i32 isLinked;
                    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
                    if (isLinked == GL_FALSE)
                    {
                        const i32 logLen = 512;
                        char log[logLen];
                        glGetProgramInfoLog(program, logLen, (GLsizei*)&logLen, log);
                        printf("%s\n", log);
                        return -1;
                    }

                    return program;
                }

                Program loadShadersAndLinkProgramFromPaths(
                    std::vector<const char*> path_to_shader_sources,
                    std::vector<u32> shader_types
                    )
                {
                    std::vector<const char*> sources;
                    std::vector<u32> compiled_shader_ids;

                    for (u32 i = 0; i < path_to_shader_sources.size(); ++i)
                    {
                        const u32 buffer_size = 1024 * 512;
                        u32 size = buffer_size;
                        char buffer[buffer_size];
                        file_system::platform_readFileIntoStringSync(path_to_shader_sources[i], buffer, &size);
                        i32 shader_id = compileShader(buffer, shader_types[i]);
                        assert(shader_id > 0);
                        compiled_shader_ids.push_back(shader_id);
                    }

                    i32 program_id = linkProgram(compiled_shader_ids);
                    assert(program_id > 0);

                    Program program = {};
                    program.id = program_id;

                    for(u32 i = 0; i < (u32) UNIFORMS::AMT; ++i){
                        i32 loc = glGetUniformLocation(program_id, unif_names[i]);
                        if(loc >= 0)
                        {
                            program.unif_locations.insert({(UNIFORMS) i, loc});
                        }
                    }
                    return program;
                }  

                u32 generateOutputTexture(i32 w, i32 h, u32 format, u32 internal_format, u32 type)
                {
                    u32 tex;
                    glGenTextures(1, &tex);

                    glBindTexture(GL_TEXTURE_2D, tex);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

                    glTexImage2D(GL_TEXTURE_2D, 0, internal_format, w, h, 0, format, type, 0);

                    glBindTexture(GL_TEXTURE_2D, 0);

                    return tex;
                }       
            }
        }   
    }
}