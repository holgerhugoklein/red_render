#pragma once

#include <glm/glm.hpp>

#include <typedefs.h>


#include "./program.h"
#include "./util.h"
#include "./glsl_paths.h"

#include "../camera.h"

namespace RED
{
    namespace render
    {
        namespace ogl
        {
            namespace forwardp
            {
                const u32 TILE_DIM = 16;
                const u32 TILE_DIM_SQ = TILE_DIM * TILE_DIM;
                const u32 MAX_AMT_LIGHTS_IN_SCENE = 20000;
                const u32 MAX_AMT_LIGHTS_IN_TILE = 1024;

                template<typename T>
                struct SSBO
                {
                    u32 buffer_id;
                    T *buffer_ptr;
                    u32 cap_in_T;

                    void pushData(T* data, u32 amt)
                    {
                        assert(amt < cap_in_T);
                        u32 size_in_bytes = amt * sizeof(T);
                        memcpy(buffer_ptr, data, size_in_bytes);
                    }

                    void bind(u32 binding_index)
                    {
                        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_index, buffer_id);
                    }
                };

                template<typename T>
                SSBO<T> createSSBO(u32 cap_in_T)
                {
                    u32 flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;

                    u32 cap_in_bytes = cap_in_T * sizeof(T);
                    u32 id;
                    glGenBuffers(1, &id);
                    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
					glBufferStorage(GL_SHADER_STORAGE_BUFFER, cap_in_bytes, 0, flags);
                    T *ptr = (T*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, cap_in_bytes, flags);

                    SSBO<T> ssbo{};
                    ssbo.buffer_id = id;
                    ssbo.buffer_ptr = ptr;
                    ssbo.cap_in_T = cap_in_T;

                    return ssbo;
                }
                
                struct ForwardPlus
                {
                    Program light_culling_prog;
                    SSBO<glm::vec4> light_pos_and_rad_ssbo;
                    SSBO<i32> light_index_list_ssbo;
                    glm::uvec2 amt_tiles;
                    u32 output_texture_id;
                };

                ForwardPlus forward_plus_data{};

                void setup(f32 w, f32 h)
                {
                    forward_plus_data.light_culling_prog = util::loadShadersAndLinkProgramFromPaths(
                        {
                            glsl_paths::forward_plus::light_culling
                        },
                        {
                            GL_COMPUTE_SHADER
                        }
                    );

                    forward_plus_data.amt_tiles = {ceil(w / TILE_DIM), ceil(h / TILE_DIM)};

                    forward_plus_data.light_pos_and_rad_ssbo = createSSBO<glm::vec4>(MAX_AMT_LIGHTS_IN_SCENE);

                    u32 total_amt_tiles = forward_plus_data.amt_tiles.x * forward_plus_data.amt_tiles.y;
                    forward_plus_data.light_index_list_ssbo = createSSBO<i32>(MAX_AMT_LIGHTS_IN_TILE * total_amt_tiles);

                    forward_plus_data.output_texture_id = util::generateOutputTexture(w, h, GL_RGBA, GL_RGBA32F, GL_FLOAT);
                }                

                void createLightListPerScreenTile(
                    u32 depth_prepass_texture_id, 
                    glm::vec4 *point_lights_pos_and_rad, 
                    u32 amt_point_lights, 
                    cam::Camera *camera,
                    glm::mat4 proj)
                {
                    forward_plus_data.light_pos_and_rad_ssbo.pushData(point_lights_pos_and_rad, amt_point_lights);
                    forward_plus_data.light_pos_and_rad_ssbo.bind(0);
                    forward_plus_data.light_index_list_ssbo.bind(1);
                    
                    glBindImageTexture(0, depth_prepass_texture_id, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
                    glBindImageTexture(1, forward_plus_data.output_texture_id, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

                    forward_plus_data.light_culling_prog.use();

                    forward_plus_data.light_culling_prog.setUniformMat4(UNIFORMS::VIEW, cam::genLookAt(camera));

                    glUniform1ui(forward_plus_data.light_culling_prog.unifLoc(UNIFORMS::AMT_LIGHTS_IN_SCENE), amt_point_lights);
                    glUniform2f(forward_plus_data.light_culling_prog.unifLoc(UNIFORMS::SCREEN_DIMS), camera->w, camera->h);
                    glUniform1f(forward_plus_data.light_culling_prog.unifLoc(UNIFORMS::FOVY), camera->fovy);
                    
                    glDispatchCompute(forward_plus_data.amt_tiles.x, forward_plus_data.amt_tiles.y, 1);

                    glMemoryBarrier(GL_ALL_BARRIER_BITS);
                }
            }
        }
    }
}