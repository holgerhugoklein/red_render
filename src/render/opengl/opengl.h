#pragma once

#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <typedefs.h>



#include "./program.h"
#include "./util.h"
#include "./glsl_paths.h"
#include "./memory.h"
#include "./forward_plus.h"

#include "../camera.h"

#include "../../assets/model_3d.h"

namespace RED
{
    namespace render 
    {
        namespace ogl
        {           
            struct DepthFramebuffer
            {
                u32 id;
                u32 depth_texture_id;
                u32 color_texture_id;
            };

            DepthFramebuffer depth_framebuffer{};

            void createDepthFramebuffer(f32 w, f32 h)
            {
                u32 id;
                glGenFramebuffers(1, &id);
                u32 depth_texture = util::generateOutputTexture(w, h, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT);
                u32 color_texture = util::generateOutputTexture(w, h, GL_RGBA, GL_RGBA32F, GL_FLOAT);
                
                glBindFramebuffer(GL_FRAMEBUFFER, id);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_texture, 0);
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_texture, 0);

                depth_framebuffer.id = id;
                depth_framebuffer.depth_texture_id = depth_texture;
                depth_framebuffer.color_texture_id = color_texture;

                assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
            }

            void createFramebuffers(f32 w, f32 h)
            {
                createDepthFramebuffer(w, h);
            }            

            struct RenderPrograms
            {
                Program depth_prepass;
                Program final_shading;
                Program textured_quat;
                Program points;
            };

            RenderPrograms programs = {};

            void createPrograms()
            {
                programs.depth_prepass = util::loadShadersAndLinkProgramFromPaths(
                    {
                        glsl_paths::depth_prepass::vert,
                        glsl_paths::depth_prepass::frag
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                programs.final_shading = util::loadShadersAndLinkProgramFromPaths(
                    {
                        glsl_paths::final_shading::vert,
                        glsl_paths::final_shading::frag
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                programs.textured_quat = util::loadShadersAndLinkProgramFromPaths(
                    {
                        glsl_paths::textured_quat::vert,
                        glsl_paths::textured_quat::frag
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                programs.points = util::loadShadersAndLinkProgramFromPaths(
                    {
                        glsl_paths::points::vert,
                        glsl_paths::points::frag
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );
            }

             void GLAPIENTRY
				oglMessageCallback(GLenum source,
					GLenum type,
					GLuint id,
					GLenum severity,
					GLsizei length,
					const GLchar* message,
					const void* userParam)
			{
                if(type == GL_DEBUG_TYPE_ERROR)
                {
                    printf_s("--------GL ERROR-----------\n%s\n", message);
                }
			}

            void initErrorCallback()
            {
				glEnable(GL_DEBUG_OUTPUT);
				glDebugMessageCallback(oglMessageCallback, 0);
            }           

            u32 light_pos_buffer = 0;
			u32 light_pos_vao = 0;

            void setupAfterContextCreation(f32 w, f32 h)
            {
                glewInit();
#ifndef NDEBUG
                initErrorCallback();
#endif

                //hacky bit to render the lights as points
                glGenVertexArrays(1, &light_pos_vao);
				glBindVertexArray(light_pos_vao);

				u32 amt_lights = 20000;
				glGenBuffers(1, &light_pos_buffer);
				glBindBuffer(GL_ARRAY_BUFFER, light_pos_buffer);
				glBufferData(GL_ARRAY_BUFFER, amt_lights * sizeof(glm::vec4), 0, GL_DYNAMIC_READ);

				glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
				glEnableVertexAttribArray(0);

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

                glClearColor(1., 0., 0., 0.);
                glEnable(GL_CULL_FACE);
                glEnable(GL_DEPTH_TEST);
                glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                mem::setup();
                createFramebuffers(w, h);
                createPrograms();
                glViewport(0, 0, w, h);
                forwardp::setup(w, h);
            }

            struct DrawElementsIndirectCommand 
			{
				GLuint count;
				GLuint instanceCount;
				GLuint firstIndex;
				GLuint baseVertex;
				GLuint baseInstance;
			};

            void printMat4(glm::mat4 mat)
            {
                printf(
                    "\n\n%f%f%f%f\n%f%f%f%f\n%f%f%f%f\n%f%f%f%f\n\n", 
                    mat[0][0], mat[0][1], mat[0][2], mat[0][3],
                    mat[1][0], mat[1][1], mat[1][2], mat[1][3],
                    mat[2][0], mat[2][1], mat[2][2], mat[2][3],
                    mat[3][0], mat[3][1], mat[3][2], mat[3][3]);
            }

            std::vector<DrawElementsIndirectCommand> generateDrawCommandsAndPushTransforms(scene::Scene* scene)
			{
				std::vector<DrawElementsIndirectCommand> drawCommands;
                u32 base_instance = 0;

				for (u32 i = 0; i < scene->models_in_scene.size(); ++i)
				{
                    assets::MODELS model = scene->models_in_scene[i];
                    std::vector<glm::mat4> model_instances = scene->model_instances[model];

                    u32 instance_count = model_instances.size();
                    mem::MeshOnMemory mesh_on_mem = mem::memory.meshesOnGPU[model];

					DrawElementsIndirectCommand indirect;
					indirect.count = mesh_on_mem.amt_indices;
					indirect.instanceCount = instance_count;
					indirect.firstIndex = 0;
					indirect.baseVertex = mesh_on_mem.vertex_offset;
					indirect.baseInstance = base_instance;

					drawCommands.push_back(indirect);

                    mem::pushPerInstanceData(model_instances.data(), model_instances.size());

                    base_instance += model_instances.size();
				}
                
				return drawCommands;
			}

            void drawSceneToBackbuffer(scene::Scene *scene)
            {
                //start by generating the commands for multidrawindirect and pushing the per 
                //instance mat4s onto the gpu memory
                //we can reuse these commands for the different render passes
                std::vector<DrawElementsIndirectCommand> drawCommands = generateDrawCommandsAndPushTransforms(scene);

                glm::mat4 view = cam::genLookAt(&scene->camera);
                glm::mat4 proj = cam::genProj(&scene->camera);

                glBindFramebuffer(GL_FRAMEBUFFER, depth_framebuffer.id);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                glBindVertexArray(mem::memory.vao);

                Program depth_prog = programs.depth_prepass;
				glUseProgram(depth_prog.id);

                depth_prog.setUniformMat4(UNIFORMS::VIEW, view);
                depth_prog.setUniformMat4(UNIFORMS::PROJECTION, proj);

                glMultiDrawElementsIndirect(
					GL_TRIANGLES,
					GL_UNSIGNED_INT,
					(void*)drawCommands.data(),
					drawCommands.size(),
					sizeof(DrawElementsIndirectCommand)
					);		

                forwardp::createLightListPerScreenTile(
                    depth_framebuffer.color_texture_id, 
                    scene->pointlight_pos_and_radius.data(), 
                    scene->pointlight_pos_and_radius.size(), 
                    &scene->camera,
                    proj);
                
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                Program final_shading_prog = programs.final_shading;
                final_shading_prog.use();
                final_shading_prog.setUniformMat4(UNIFORMS::VIEW, view);
                final_shading_prog.setUniformMat4(UNIFORMS::PROJECTION, proj);

                glUniform2f(final_shading_prog.unifLoc(UNIFORMS::SCREEN_DIMS), scene->camera.w, scene->camera.h);

                glMultiDrawElementsIndirect(
					GL_TRIANGLES,
					GL_UNSIGNED_INT,
					(void*)drawCommands.data(),
					drawCommands.size(),
					sizeof(DrawElementsIndirectCommand)
					);		

                //draw lights as points	
				glBindBuffer(GL_ARRAY_BUFFER, light_pos_buffer);
				glBufferSubData(GL_ARRAY_BUFFER,
					0,
					scene->pointlight_pos_and_radius.size() * sizeof(glm::vec4),
					scene->pointlight_pos_and_radius.data());

				glBindVertexArray(light_pos_vao);

                programs.points.use();
                programs.points.setUniformMat4(UNIFORMS::VIEW, view);
                programs.points.setUniformMat4(UNIFORMS::PROJECTION, proj);						
				
				glPointSize(5.);

				glDrawArrays(GL_POINTS, 0, scene->pointlight_pos_and_radius.size());
#if 1
                Program tex_quat_prog = programs.textured_quat;
                glUseProgram(tex_quat_prog.id);

                glClear(GL_DEPTH_BUFFER_BIT);

                glBindTexture(GL_TEXTURE_2D, forwardp::forward_plus_data.output_texture_id);
                glDrawArrays(GL_TRIANGLES, 0, 6);
#endif
                mem::clearPerInstanceData();
            }    
        }
    }
}