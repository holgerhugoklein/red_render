#pragma once

#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

#include <typedefs.h>

#include "program.h"
#include "util.h"
#include "glsl_paths.h"
#include "gpu_buffer.h"

#include "../../assets/model_3d.h"

namespace RED
{
    namespace render 
    {
        namespace ogl
        {
            namespace mem
            {
                struct MeshOnMemory
                {
                    u32 amt_vertices;
                    u32 vertex_offset;

                    u32 amt_indices;
                    u32 index_offset;
                };            

                struct Memory
                {
                    buf::Per_Instance_Buffer per_instance_buffer;

                    buf::Non_Dynamic_Buffer<glm::vec3> vertex_position_buffer;
                    buf::Non_Dynamic_Buffer<glm::vec3> vertex_normals_buffer;
                    buf::Non_Dynamic_Buffer<u32> index_buffer;

                    std::unordered_map<assets::MODELS, MeshOnMemory> meshesOnGPU;

                    u32 vao;
                };

                Memory memory = {};

                void pushMeshOntoGPUMemory(assets::MODELS model, assets::Mesh *mesh)
                {   
                    u32 vertex_offset = buf::pushDataOntoNonDynamicBufferReturnOffset<glm::vec3>(
                        &memory.vertex_position_buffer,
                        mesh->vertices.data(),
                        mesh->vertices.size(),
                        GL_ARRAY_BUFFER);

                    buf::pushDataOntoNonDynamicBufferReturnOffset<glm::vec3>(
                        &memory.vertex_normals_buffer,
                        mesh->normals.data(), 
                        mesh->normals.size(),
                        GL_ARRAY_BUFFER);

                    u32 index_offset = buf::pushDataOntoNonDynamicBufferReturnOffset<u32>(
                        &memory.index_buffer,
                        mesh->indices.data(),
                        mesh->indices.size(),
                        GL_ELEMENT_ARRAY_BUFFER
                    );
                    
                    MeshOnMemory meshOnMemory = {};
                    meshOnMemory.amt_indices = mesh->indices.size();
                    meshOnMemory.index_offset = index_offset;

                    meshOnMemory.amt_vertices = mesh->vertices.size();
                    meshOnMemory.vertex_offset = vertex_offset;

                    memory.meshesOnGPU.insert({model, meshOnMemory});
                }

                void pushPerInstanceData(glm::mat4 *data, u32 size)
                {
                    buf::pushPerInstanceDataReturnOffset(&memory.per_instance_buffer, data, size);
                }

                void clearPerInstanceData()
                {
                    memory.per_instance_buffer.size_in_mat4 = 0;
                }

                void setup()
                {
                    glGenVertexArrays(1, &memory.vao);
                    glBindVertexArray(memory.vao);

                    memory.vertex_position_buffer = buf::generateNonDynamicBuffer<glm::vec3>(1024 * 1024, GL_ARRAY_BUFFER);
                    memory.vertex_normals_buffer = buf::generateNonDynamicBuffer<glm::vec3>(1024 * 1024, GL_ARRAY_BUFFER);
                    memory.index_buffer = buf::generateNonDynamicBuffer<u32>(1024 * 1024, GL_ELEMENT_ARRAY_BUFFER);
                    
                    memory.per_instance_buffer = buf::generatePerInstanceBuffer(1024 * 64);

                    //set up vertex attribute descriptors
                    glBindBuffer(GL_ARRAY_BUFFER, memory.vertex_position_buffer.id);
                    glVertexAttribPointer((u32) SHADER_ATTR::A_POS, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_POS);

                    glBindBuffer(GL_ARRAY_BUFFER, memory.vertex_normals_buffer.id);
                    glVertexAttribPointer((u32) SHADER_ATTR::A_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_NORMAL);    

                    glBindBuffer(GL_ARRAY_BUFFER, memory.per_instance_buffer.id);
                    glVertexAttribPointer((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*) 0);
                    glVertexAttribPointer((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 1, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*) sizeof(glm::vec4));
                    glVertexAttribPointer((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*) (2 * sizeof(glm::vec4)));
                    glVertexAttribPointer((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*) (3 * sizeof(glm::vec4)));

                    glVertexAttribDivisor((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4, 1);
                    glVertexAttribDivisor((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 1, 1);
                    glVertexAttribDivisor((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 2, 1);
                    glVertexAttribDivisor((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 3, 1);     

                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4);
                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 1);
                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 2);
                    glEnableVertexAttribArray((u32) SHADER_ATTR::A_PER_INSTANCE_MAT4 + 3);       
                }
            }
        }
    }
}