#pragma once

#include <typedefs.h>

namespace RED
{
    namespace render 
    {
        namespace ogl
        {
            enum class SHADER_ATTR
            {
                A_POS,
                A_NORMAL,
                A_PER_INSTANCE_MAT4,
                AMT
            };

            const char * attr_names[] = {
                "a_pos",
                "a_normal",
                "a_per_instance_mat4"
            };

            enum class UNIFORMS
            {
                VIEW,
                PROJECTION,
                SCREEN_DIMS,
                FOVY,
                AMT_LIGHTS_IN_SCENE,
                AMT
            } ;

            const char * unif_names[] = {
                "view",
                "projection",
                "screen_dims",
                "fovy",
                "amt_lights_in_scene"
            };

            struct Program
            {
                u32 id;
                std::unordered_map<UNIFORMS, u32> unif_locations;

                void use()
                {
                    glUseProgram(id);                    
                }

                void setUniformMat4(UNIFORMS uniform, glm::mat4 mat4)
                {
                    glUniformMatrix4fv(unif_locations[uniform], 1, GL_FALSE, glm::value_ptr(mat4));
                }

                u32 unifLoc(UNIFORMS unif)
                {
                    return unif_locations[unif];
                }
            };
        }
    }
}