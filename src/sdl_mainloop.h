#pragma once
#include <sdl2/SDL.h>

#include <typedefs.h>

#include "./render/render.h"

#include "input/sdl_input.h"
#include "input/input.h"

namespace RED
{
    struct SDLTimer
    {
        u64 perf_freq;
    };

    SDLTimer timer;

    u64 ticks()
    {
        return SDL_GetPerformanceCounter();
    }

    f64 durationS(u64 tick0, u64 tick1)
    {
        return ((f64) 1000000 * (tick1 - tick0)) / ((f64) 1000000 * timer.perf_freq);
    }

    f32 randBetween(f32 min, f32 max)
    {
        return ((f32) rand() / (f32) RAND_MAX) * (max - min) + min;
    }

    void sdl_mainloop(SDL_Window *window, f32 w, f32 h)
    {
        u64 perf_freq = SDL_GetPerformanceFrequency();
        timer = { perf_freq };

        input::SDL_Input sdl_input = input::setup();
        b32 keep_running = true;
        render::render_pushMeshOntoGPU(assets::MODELS::HAPPY_BUDDHA, &assets::assets.meshes[assets::MODELS::HAPPY_BUDDHA]);
        
        render::scene::Scene scene;
        render::scene::addModel(&scene, assets::MODELS::HAPPY_BUDDHA);       

        for(u32 i = 0; i < 55; ++i)
        {
            glm::vec3 pos{(f32) (i % 5), 0, -floor((f32) i / 5.f)};
            pos *= .2;
            render::scene::pushModelInstance(&scene, assets::MODELS::HAPPY_BUDDHA, glm::translate(glm::mat4(1), pos));
        }

        const u32 AMT_LIGHTS = 1024 * 2;
        std::vector<glm::vec3> light_vels;
        light_vels.reserve(AMT_LIGHTS);

        for(u32 i = 0; i < AMT_LIGHTS; ++i)
        {
            glm::vec4 light = {
                randBetween(-.1, 1.),
                randBetween(0., .3),
                randBetween(-2.5, 0.),
                .1
            };
            scene.pointlight_pos_and_radius.push_back(light);

            glm::vec3 vel{0., 0., randBetween(-.05, .05)};
            light_vels.push_back(vel);
        }

        scene.camera = render::cam::setupCamera(glm::vec3{.2, .3, .3}, glm::vec3{0, 0, -1}, glm::radians(45.), w, h);
            
        u64 start = ticks();
        f64 lastFrameS = 1. / 60.;

        while(keep_running)
        {
            start = ticks();
            input::Input input = input::generateFromSDL(&sdl_input);
            render::cam::move(&scene.camera, input::getMoveVec(input), lastFrameS);
            render::cam::rotate(&scene.camera, input.rotCameraCmds.xy, lastFrameS);

            for(u32 light = 0; light < AMT_LIGHTS; ++light)
            {
                glm::vec4 vel_to_add = glm::vec4((f32) lastFrameS * light_vels[light], 0.);
                scene.pointlight_pos_and_radius[light] = scene.pointlight_pos_and_radius[light] + vel_to_add;
                if(scene.pointlight_pos_and_radius[light].z > 0.5)
                {
                    scene.pointlight_pos_and_radius[light].z = -2.5;
                }
                else if(scene.pointlight_pos_and_radius[light].z < -2.5)
                {
                    scene.pointlight_pos_and_radius[light].z = 0;
                }
            }
            
            if(input.stop_running) return;
            render::render_drawSceneToBackbufferAndPresent(&scene, window);
            
            u64 end = ticks();
            lastFrameS = durationS(start, end);
            printf("dur: %f\n", lastFrameS);
        }
    }
}
