#pragma once

#include <unordered_map>

#include "assimp_load_model.h"
#include "model_paths.h"

namespace RED
{
    namespace assets
    {
        struct Assets
        {
            std::unordered_map<MODELS, Mesh> meshes;
        };
        
        Assets assets = {};

        void setup()
        {
            for (u32 i = 0; i < (u32) MODELS::AMT; ++i)
            {
                Mesh mesh = assimp_imp::loadFirstMeshFromScene(model_paths[(MODELS) i]);
                assets.meshes.insert({(MODELS) i, mesh});
            }
        }

    }   
}