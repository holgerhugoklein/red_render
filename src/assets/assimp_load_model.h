#pragma once

#include <assert.h>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>

#include <typedefs.h>

#include "./model_3d.h"

namespace RED
{
    namespace assets
    {
		namespace assimp_imp
		{
			glm::vec3 assimpVec3ToGlm(aiVector3D vec)
			{
				return { vec.x, vec.y, vec.z };
			}	

			Mesh assimpMeshToMesh(aiMesh *assimp_mesh, const aiScene* scene)
			{
				std::vector<glm::vec3> vertices;
				std::vector<glm::vec3> normals;
				std::vector<glm::vec2> tex_coords;
				std::vector<glm::vec3> tangents;
				std::vector<glm::vec3> bitangents;

				for (u32 i = 0; i < assimp_mesh->mNumVertices; ++i) {
					vertices.push_back(assimpVec3ToGlm(assimp_mesh->mVertices[i]));

					if(assimp_mesh->mNormals)
						normals.push_back(assimpVec3ToGlm(assimp_mesh->mNormals[i]));

					if (assimp_mesh->mTextureCoords[0]) {
						glm::vec2 vec;
						vec.x = assimp_mesh->mTextureCoords[0][i].x;
						vec.y = assimp_mesh->mTextureCoords[0][i].y;
						tex_coords.push_back(vec);
					}

					if(assimp_mesh->mTangents)
						tangents.push_back(assimpVec3ToGlm(assimp_mesh->mTangents[i]));
					if (assimp_mesh->mBitangents)
						bitangents.push_back(assimpVec3ToGlm(assimp_mesh->mBitangents[i]));
				}

				std::vector<u32> indices;
				for (u32 i = 0; i < assimp_mesh->mNumFaces; ++i) {
					aiFace face = assimp_mesh->mFaces[i];
					for (u32 j = 0; j < face.mNumIndices; j++)
						indices.push_back(face.mIndices[j]);
				}

				return { vertices, normals, tex_coords, tangents, bitangents, indices };
			}
				

			Mesh loadFirstMeshFromScene(const char * path)
			{
				Assimp::Importer importer;
				const aiScene* scene = importer.ReadFile(
					path, 
					aiProcess_Triangulate | 
					aiProcess_FlipUVs | 
					aiProcess_GenNormals | 
					aiProcess_GenUVCoords);

				assert(scene);
				aiMesh *assimp_mesh = scene->mMeshes[0];
				return assimpMeshToMesh(assimp_mesh, scene);
			}       
		}
    }
}