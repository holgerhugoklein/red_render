#pragma once

#include <unordered_map>

namespace RED
{
    namespace assets
    {
        enum class MODELS
        {
            HAPPY_BUDDHA,
            AMT
        };

        std::unordered_map<MODELS, const char*> model_paths = 
        {
            { MODELS::HAPPY_BUDDHA, "./models/happy_buddha.obj" }
        };
    }   
}