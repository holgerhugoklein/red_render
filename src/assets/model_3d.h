#pragma once

#include <vector>
#include <glm/glm.hpp>

#include <typedefs.h>

namespace RED
{
    namespace assets
    {
        struct Mesh
        {
		    std::vector<glm::vec3> vertices;
			std::vector<glm::vec3> normals;
			std::vector<glm::vec2> tex_coords;
			std::vector<glm::vec3> tangents;
			std::vector<glm::vec3> bitangents;
			std::vector<u32> indices;
        };
    }
}