#pragma once

#include <typedefs.h>

namespace RED
{
    namespace assets
    {
        namespace util
        {
            //https://stackoverflow.com/questions/7666509/hash-function-for-string
            u64 hashStr(const char* str)
            {
                u64 hash = 5381;
                i32 c;
                while(c = *str++)
                {
                    hash = ((hash << 5) + hash) + c;
                }
                return hash;
            }
        }
    }
}