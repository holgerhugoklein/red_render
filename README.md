This is an opengl implementation of forward+ / tiled forward shading. 
It also uses some AZDO flavored stuff such as multidrawindirect and
persistently mapped buffers.

forward+:https://www.3dgep.com/forward-plus/
AZDO: https://www.khronos.org/assets/uploads/developers/library/2014-gdc/Khronos-OpenGL-Efficiency-GDC-Mar14.pdf


It is meant to be used by people who want to implement their own version
of am azdo/forward+ renderer using opengl as a jumping off point.
Also, if you someone wants to learn more about opengl compute shaders,
this is also a great coding challenge.

Theres a lot of small annoyances that get in the way when doing advanced stuff
with opengl such as figuring out how to exactly pass textures or link
ssbos. This repo can be used to quickly copy and paste the relevant code.

I will probably move on to using vulkan in the future, so this repo wont be 
updated.

The test scene renders 55 happy buddhas instanced with 2048 point lights 
at a comfortable 60fps on my integrated NVIDIA GTX 1060,
and at 40fps with 2048 * 2 lights, and at 25 fps with 2048 * 4 lights.
All this is in the debug build. Since this was more for educational purposes,
there is a lot that could be further optimized.

another implementation can be found here https://github.com/bcrusco/Forward-Plus-Renderer

2048 lights: 
![](test_scene_1024_lights.PNG)

~4000 lights:
![](test_scene_4000_lights.PNG)

~9000 lights:
![](test_scene_9000_lights.PNG)
