@echo off

call "D:\Visual studio\VC\Auxiliary\Build\vcvars64.bat"

set commonCompilerFlags=/nologo /Od /Zi /WX /wd4127 /wd4100 /wd4101 /wd4459 /wd4189 /EHsc

set commonLinkerFlags=/link /LIBPATH:"../extern/lib/x64" /LIBPATH:%VCPKG_LIB%

set libs=SDL2.lib SDL2main.lib glew32.lib opengl32.lib Shell32.lib assimp-vc142-mt.lib

set otherincludes=/I../extern/includes /I%VCPKG_INCLUDE% /I%RED_FILESYSTEM_SRC% 

mkdir build
pushd build
echo j|del *.*

xcopy ..\extern\lib\x64\SDL2.dll .\

cl /DSLOW=1  %otherincludes% %commonCompilerFlags% ../src/win32_main.cpp  %commonLinkerFlags%  %libs% /SUBSYSTEM:CONSOLE 

::devenv win32_main.exe

popd

echo win32 done
